import React,{useState} from 'react';
import SearchBar from './Components/SearchBar'
import { IUserInput } from './Common/Interfaces';
import MediaGrid from './Components/MedieGrid';



function App() {
  const [UserInput, setUserInput] = useState<IUserInput>({
    SearchQuery: "Star war"
  });
  function SetUserInput(a: IUserInput) {
    setUserInput(a);    
  }
  return (
    <div className="App">
      <header className="App-header">
        <SearchBar SetUserInput={(a: IUserInput) => SetUserInput(a)}/>
        <MediaGrid SearchQuery = {UserInput.SearchQuery}/>

      </header>
    </div>
  );
}

export default App;
