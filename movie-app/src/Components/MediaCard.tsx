import React from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

interface IMediaCardProps {
    Poster: string | undefined;
    Title: string | undefined;
    Year:string | undefined;
    imdbID:string | undefined;
}

function MediaCard(props: IMediaCardProps) {
    return (
        <div>
            <Card className="MediaCardContainer">
                <CardActionArea>
                    <CardMedia
                        component="img"
                        height="300"
                        className="MediaCardImage"
                        image={props.Poster}
                    />
                    <CardContent>
                        <h3>{props.Title}</h3>
                        <Typography variant="body2" color="textSecondary" component="p" className="MediaCardDescription">
                            
                            {props.Year}
                           IMDB ID: {props.imdbID}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        </div>
    )
}

export default MediaCard