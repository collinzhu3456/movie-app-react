import React, { useState, useEffect } from 'react';
import { Grid } from '@material-ui/core';
import MediaCard from './MediaCard'


interface IState {
    Title: any;
    Year: any;
    Poster:any;
    imdbID:any;
}
interface IMediaGridProps {
    SearchQuery: (string | null);
}
function MediaGrid(props: IMediaGridProps) {
    const [ItemArray, setItemArray] = useState<IState[]>([{Title: "",Year: "",Poster:"",imdbID:""}])
    useEffect(() => {
        fetch('http://www.omdbapi.com/?s='+ props.SearchQuery + '&apikey=50659c3f')
            .then(response => response.json())
            .then(res => {
                setItemArray(res.Search)
                console.log(res.Search)
            })
            .catch(() => console.log("it didn't work")
            );

    }, [props.SearchQuery]);

    // Poster: string | undefined;
    // Title: string | undefined;
    // Year:string | undefined;
    // imdbID:string | undefined;
    var Cards: JSX.Element[] = [];
    ItemArray?.forEach((el: IState, i: Number) => {
        if (!el || !el.Title || !el.Poster||!el.Year||!el.imdbID) {
            return;
        }
        Cards.push(
            <Grid key={"card_"+i} item sm={6} md={4} lg={3} className="MediaGridCard">
                <MediaCard Title = {el.Title} Year = {el.Year} Poster = {el.Poster} imdbID = {el.imdbID}/>
            </Grid>)
    })
    return (
        <div>
            <Grid container spacing={3} className="MediaGridContainer">
                {Cards}
            </Grid>
        </div>
    )
}



//http://www.omdbapi.com/?s=THE+MATRIX&apikey=50659c3f
export default MediaGrid